<!DOCTYPE HTML>
<html>
    <head>
        <title>IM Dev Days</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
    </head>
    <body>
        <div id="content">
            <div class="inner">
                <article class="box post post-excerpt">
                    <header>
                        <h2><a href="#">Dev day show and tell</a></h2>
                    </header>
                    <p>
                        We’re looking to re-start our dev day show and tell sessions. If you have a topic you think you
                        can lead a 20(ish) minute presentation on then please volunteer! Likewise, let us know if
                        there’s anything you’d be really interested to hear about, we can likely volunteer a speaker.
                    </p>
                    <p>
                        Possible topics could be:
                        <ul>
                            <li>Innovative solutions to problems</li>
                            <li>New developments recently completed</li>
                            <li>Upcoming new technologies</li>
                            <li>New tools or ways of working</li>
                        </ul>
                    </p>
                    <p>
                        For now, use the <a href="mailto:dl-tech-leads@immediate.co.uk">dl-tech-leads@immediate.co.uk</a>
                        list to send any suggestions. They will be collated on Confluence along with links to recordings
                        from previous sessions.
                    </p>
                </article>
            </div>
        </div>
        <div id="sidebar">
            <h1 id="logo"><a href="#">DEVDAYS</a></h1>
            <nav id="nav">
                <ul>
                    <li class="current"><a href="#">Home</a></li>
                    <li><a href="#">Submit a talk</a></li>
                    <li><a href="#">Suggest a topic</a></li>
                </ul>
            </nav>
            <section class="box search">
                <form method="post" action="#">
                    <input type="text" class="text" name="search" placeholder="Search" />
                </form>
            </section>
            <section class="box recent-posts">
                <header>
                    <h2>Previous Talks</h2>
                </header>
                <ul>
                    <li><a href="#">MS FrontPage basics</a></li>
                    <li><a href="#">PHP6 conspiracy</a></li>
                    <li><a href="#">Intentional placeholder</a></li>
                    <li><a href="#">Tomato: Fruit or vegetable?</a></li>
                </ul>
            </section>
            <section class="box calendar">
                <div class="inner">
                    <table>
                        <caption>September 2016</caption>
                        <thead>
                            <tr>
                                <th scope="col" title="Monday">M</th>
                                <th scope="col" title="Tuesday">T</th>
                                <th scope="col" title="Wednesday">W</th>
                                <th scope="col" title="Thursday">T</th>
                                <th scope="col" title="Friday">F</th>
                                <th scope="col" title="Saturday">S</th>
                                <th scope="col" title="Sunday">S</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="3" class="pad"><span>&nbsp;</span></td>
                                <td><span>1</span></td>
                                <td class="today"><a href="#">2</a></td>
                                <td><span>3</span></td>
                                <td><span>4</span></td>
                            </tr>
                            <tr>
                                <td><span>5</span></td>
                                <td><span>6</span></td>
                                <td><span>7</span></td>
                                <td><span>8</span></td>
                                <td><a href="#">9</a></td>
                                <td><span>10</span></td>
                                <td><span>11</span></td>
                            </tr>
                            <tr>
                                <td><span>12</span></td>
                                <td><span>13</span></td>
                                <td><span>14</span></td>
                                <td><span>15</span></td>
                                <td><span>16</span></td>
                                <td><span>17</span></td>
                                <td><span>18</span></td>
                            </tr>
                            <tr>
                                <td><span>19</span></td>
                                <td><span>20</span></td>
                                <td><span>21</span></td>
                                <td><span>22</span></td>
                                <td><a href="#">23</a></td>
                                <td><span>24</span></td>
                                <td><span>25</span></td>
                            </tr>
                            <tr>
                                <td><span>26</span></td>
                                <td><span>27</span></td>
                                <td><span>28</span></td>
                                <td><span>29</span></td>
                                <td><span>30</span></td>
                                <td class="pad" colspan="2"><span>&nbsp;</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>
            <ul id="copyright">
                <li>&copy; IM Dev Days</li>
            </ul>
        </div>
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>
	</body>
</html>
